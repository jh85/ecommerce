import pytest
from pytest_package.driver_setup import DriverSetup


@pytest.fixture()
def setup():
    ds = DriverSetup()
    ds.set_driver
    yield
    ds.driver.quit()


# @pytest.fixture(scope='class')
# def one_time_setup():
#     ds = DriverSetup()
#     ds.set_driver
#     yield
#     ds.driver.quit()
