from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver.support.wait import WebDriverWait as Wait

from pytest_package.driver_setup import DriverSetup


class UtilFunctions(DriverSetup):

    @staticmethod
    def wait_until_element_clickable(by, locator, timeout=30):
        return Wait(DriverSetup.driver, timeout).until(ec.element_to_be_clickable((by, locator)))

    @staticmethod
    def wait_until_text_to_be_present_in_element(by, locator, text, timeout=30):
        return Wait(DriverSetup.driver, timeout).until(ec.text_to_be_present_in_element((by, locator), text))
