import pytest
from pytest_package.page_objects import PageObjects
from pytest_package.util_functions import UtilFunctions as Util


@pytest.mark.usefixtures('setup')
class TestCases(PageObjects):

    def test_positive(self):
        try:
            self.btn_region_us().click()
            self.btn_no().click()
            self.btn_ok().click()
            self.btn_menu().click()
            self.btn_menu_men().click()
            self.btn_menu_new().click()
            self.btn_prod().click()
            self.btn_size().click()
            self.btn_size_xs().click()
            self.btn_add().click()
            # assert self.btn_cart_count().text == str(1)
            dict_cart = self.lb_cart()
            # assertion - wait until 1 item is showing in the cart
            Util.wait_until_text_to_be_present_in_element(dict_cart.get('loc_type'), dict_cart.get('loc_val'), str(1))
        except Exception:
            print('error %s: ' % Exception)



