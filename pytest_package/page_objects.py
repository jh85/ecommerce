from selenium.webdriver.common.by import By
from pytest_package.driver_setup import DriverSetup
from pytest_package.util_functions import UtilFunctions as Util


class PageObjects(DriverSetup):

    def btn_region_us(self):
        return self.driver.find_elements_by_id("com.hm.goe:id/marketName")[1]

    def btn_no(self):
        return Util.wait_until_element_clickable(By.ID, 'android:id/button2')

    def btn_ok(self):
        return Util.wait_until_element_clickable(By.ID, 'android:id/button1')

    def btn_menu(self):
        return Util.wait_until_element_clickable(By.XPATH, '//android.view.ViewGroup /android.widget.ImageButton')

    def btn_menu_men(self):
        return Util.wait_until_element_clickable(By.XPATH, "//android.widget.TextView[@text='Men']")

    def btn_menu_new(self):
        return Util.wait_until_element_clickable(By.XPATH, "//android.widget.TextView[@text='New Arrivals']")

    def btn_prod(self):
        return Util.wait_until_element_clickable(By.ID, 'com.hm.goe:id/element1')

    def btn_size(self):
        return Util.wait_until_element_clickable(By.ID, 'com.hm.goe:id/selectSizeContainer')

    def btn_size_xs(self):
        return Util.wait_until_element_clickable(By.XPATH, '//androidx.recyclerview.widget.RecyclerView/android.view.ViewGroup[1]/android.widget.TextView[1]')

    def btn_add(self):
        return Util.wait_until_element_clickable(By.ID, 'com.hm.goe:id/addToBagButton')

    def btn_cart_count(self):
        return Util.wait_until_element_clickable(By.ID, 'com.hm.goe:id/hm_shoppingbag_count')

    def lb_cart(self):
        return {'loc_type': By.ID, 'loc_val': 'com.hm.goe:id/hm_shoppingbag_count'}
