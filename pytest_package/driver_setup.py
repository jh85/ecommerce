import os
from os.path import join
from appium import webdriver


class DriverSetup:
    driver = None

    @property
    def set_driver(self):
        try:
            dc = {
                'platformName': 'Android',
                'deviceName': 'emulator-5554',
                'appPackage': 'com.hm.goe',
                'appWaitActivity': 'com.hm.goe.app.marketselector.MarketSelectorActivity',
                'app': join(os.path.dirname(__file__), 'com.hm.goe_2019-10-31.apk')
            }
            DriverSetup.driver = webdriver.Remote('http://localhost:4723/wd/hub', dc)
            return self.driver
        except Exception:
            print('error %s: ' % Exception)
